var m_SlopScalingFactor = 15;//scaling factor for span queries in this case 15because 15 kmer lengths are indexed therefore there is a gap of 15 between each kmer of n length
var m_geneLocationDataUrl = "server/getgene2.php?id=";
var m_genesUrl = "server/getgene3.php?arr=";

var client;
var results = [];
var order = [];
var ID = null;

$(function () {
    client = new $.es.Client({
        hosts: ['esearchqut01b.cloudapp.net:9200',
        'http://esearchqut02b.cloudapp.net:9200/',
        'http://esearchqut03b.cloudapp.net:9200/'],
        apiVersion: "1.1"
    });
    client.ping({
        requestTimeout: 10000,
    }, function (error) {
        if (error) {
            document.write('elasticsearch cluster is down! ' + error);
        } else {
            $('#status').text("Server Connection Established!").css('background-color', 'lightgreen');
        }
    });
});

var m_KmerMinLength = 5;
var m_KmerMaxLength = 20;
var kmerValidChars = /^[actgACTG]+$/;
var numberValidChars = /^[0-9]+$/;

function verifyKmer(input) {
    if (input.length < m_KmerMinLength || input.length > m_KmerMaxLength)
        return false;
    return kmerValidChars.test(input);
}

function verifyKmers(input) {
    var arr = input.split(" ");
    for (var i = 0; i < arr.length; ++i) {
        if (!verifyKmer(arr[i]))
            return false;
    }
    return true;
}

function verifyNumber(input) {
    return numberValidChars.test(input);
}

function verifiySpanQury(input) {
    var arr = input.split(" ");
    if (arr.length < 3)
        return false;
    for (var i = 0; i < arr.length - 1; ++i) {
        if (!verifyKmer(arr[i]))
            return false;
    }
    return verifyNumber(arr[arr.length - 1]);
}

function processSearchResult(error, response) {
    if (error) {
        console.error(error);
        $('#kmer-result-list').html(m_noResultsMsg);
        finishedLoading();
        return;
    }
    res = response;
    window.history.replaceState({ results: response });//add content to history
    //$("body").append(JSON.stringify(response.hits.hits));
    $('#kmer-results').show();
    if (response.hits.hits.length == 0) {
        //todo
        $('#kmer-result-list').html(m_noResultsMsg);
        finishedLoading();
        return;
    }
    AddPagination(response.hits.total);
    $.each(response.hits.hits, function (index, val) {
        addResult(val, index);
    });

    finishedLoading();
}

function searchGeneral(queryBody, termFrequency, timeout) {
    var query = {
        index: 'documents',
        type: 'kmer',
        body: queryBody,
        from: CurrentPage * 10,
        _sourceExclude: "content"
    };

    if (timeout) {
        query.requestTimeout = timeout;
    }

    if (termFrequency) {
        query.term_frequency = termFrequency;
    }

    loading();
    client.search(query, processSearchResult);
}

function buildMatchQueryBody(input)
{
    var arr = input.split(" ");

    var body = {
        query: {
            bool: {
                must: []
            }
        }
    }

    for (var i = 0; i < arr.length; ++i) {
        body.query.bool.must.push({ "match": { "content": arr[i] } });
    }
    return body;
}

function searchBio(input) {
    if (!verifyKmers(input)) {
        alert("kmers must each be between " + m_KmerMinLength + " and " + m_KmerMaxLength + " charecters and contain only the letters: A T C G");
        return false;
    }
    SearchTerm = input;
    
    var body = buildMatchQueryBody(input)

    searchGeneral(body, TermFrequency);
    return true;
}

function searchSpanBio(input) {

    if (!verifiySpanQury(input))
    {
        alert("kmers must each be between " + m_KmerMinLength + " and " + m_KmerMaxLength + " charecters and contain only the letters: A T C G and a number must be at the end representing the maximum span");
        return false;
    }
    SearchTerm = input;

    var arr = input.split(" ");
    var slop = +arr[arr.length - 1];

    var body = {
        query: {
            "span_near": {
                "clauses": [],
                "slop": (slop+1) * m_SlopScalingFactor,
                "in_order": false,
                "collect_payloads": false
            }
        }
    }

    for (var i = 0; i < arr.length - 1; ++i) {
        body.query.span_near.clauses[i] = { "span_term": { "content": arr[i] } };
    }

    searchGeneral(body, TermFrequency);
    return true;
}

function searchSimiler(input) {

    if (!verifyKmers(input))
    {
        alert("kmers must each be between " + m_KmerMinLength + " and " + m_KmerMaxLength + " charecters and contain only the letters: A T C G and a number must be at the end representing the maximum span");
        return false;
    }
    SearchTerm = input;

    var body = {
        query: {
			"more_like_this_field" : {
				"content" : {
					percent_terms_to_match: 0.1,
					analyzer: "kmer",
					"like_text" : input,
					"min_term_freq" : 0,
					min_doc_freq: 0,
				}
			}
		}
    }


    searchGeneral(body);

    return true;
}

function searchSimilerFuzzy(input) {

    if (!verifyKmers(input))
    {
        alert("kmers must each be between " + m_KmerMinLength + " and " + m_KmerMaxLength + " charecters and contain only the letters: A T C G and a number must be at the end representing the maximum span");
        return false;
    }
    SearchTerm = input;

    var body = {
        query: {
			"fuzzy_like_this_field" : {
				"content" : {
					analyzer: "kmer",
					ignore_tf : true,
                    fuzziness: 1,
					"like_text" : input
				}
			}
		}
    }

    searchGeneral(body, false, 1000000);
    return true;
}

//adds the give kmer reult "data" to the display
function addResult(data, index) {
    results[data._id] = data._source;
    results[data._id].term_frequency = data.term_frequency;
    order[CurrentPage * 10 + index] = data._id;
    var str = "<div class='media kmer-result' id='" + data._id + "' >";
    str += '<div class="media-body"><h4 class="media-heading">' + data._source.title + '</h4>';
    str += '<b>File Name:</b> ' + data._source.filename;

    if (data.term_frequency) {
        for (var i in data.term_frequency) {
            str += '<br /><b>Term:</b> ' + data.term_frequency[i].term + ", <b>Term:</b> " + data.term_frequency[i].frequency;
        }
    }
    str += "</div></div>";
    $('#kmer-result-list').append(str);
}

function kmerInfo(id) {
    ID = id;
    data = results[id];
    if (data) {
        displayKmerHeading(data)
    }
    if (SearchTerm)
    {
        getContent(id);
    } else {
        getContentNoSearchTerm(id);
    }
}

function displayKmerHeading(data) {
    $('#kmer-results').hide();
    $('#kmer-info').show();
    $('.jumbotron h1').html(data.title);
    var headings = "<p><b>File Name:</b> " + data.filename + "</p>"
    headings += "<p><b>Indexed Date:</b> " + data.postDate + "</p>";
    $('.jumbotron .lead').html(headings);
    $('#detailed-description').show();
}

function processGetContentResponse(error, response) {
    if (error) {
        console.error(error);
        $('#wikipedia').text("Content not loaded. Error:" + error);
        finishedLoading();
        return;
    }
    cont = response;//make avalable fore debuging
    var content = response.hits.hits[0];
    processContentResponse(content);
}

function processContentResponse(content, genes)
{
    var id = content._id;
    window.history.replaceState({ content: content });//add content to history
    displayKmerHeading(content._source);
    results[id] = content._source;
    content.term_vectors.sort(function (a, b) { return a.start_position - b.start_position });
    results[id].term_vectors = content.term_vectors;
    $('#wikipedia').text(results[id].content);
    highlight(id);
    if (genes)
    {
        results[id].genes = genes;
    }
    getGeneLocationData(id);
    finishedLoading();
}

function getContent(id) {
    if (QueryType == "span")
        return getContentSpan(id)
    if (QueryType == "similarity" || QueryType == "similarityFuzzy")
        return getContentNoSearchTerm(id)


    var body = buildMatchQueryBody(SearchTerm);

    body.query.bool.must.unshift({ "match": { "_id": id } });

    loading();
    $('#wikipedia').text("Loading...");
    client.search({
        index: 'documents',
        type: 'kmer',
        id: id,
        terms: true,
        body: body,
        requestTimeout: 100000,
    }, processGetContentResponse);
}

function getContentSpan(id)
{
    var arr = SearchTerm.split(" ");
    var slop = +arr[arr.length - 1];

    var body = {
        query: {
            bool: {
                must: [{ "match": { "_id": id } },
                    {
                    "span_near": {
                        "clauses": [],
                        "slop": (slop+1) * m_SlopScalingFactor,
                        "in_order": false,
                        "collect_payloads": false
                    }
                }]
            }
        }
    }

    for (var i = 0; i < arr.length - 1; ++i) {
        body.query.bool.must[1].span_near.clauses[i] = { "span_term": { "content": arr[i] } };
    }

    loading();
    $('#wikipedia').text("Loading...");
    client.search({
        index: 'documents',
        type: 'kmer',
        terms: true,
        body: body,
        requestTimeout: 100000,
    }, processGetContentResponse);
}

function getContentNoSearchTerm(id) {
    if (results[id] && results[id].content) {
        $('#wikipedia').text(results[id].content);
        return;
    }

    loading();
    $('#wikipedia').text("Loading...");
    client.get({
        index: 'documents',
        type: 'kmer',
        id: id,
        requestTimeout: 30000,
    }, processGetContentResponseNoSearch);
}

function processGetContentResponseNoSearch(error, response) {
    if (error) {
        console.error(error);
        $('#wikipedia').text("Content not loaded. Error:" + error);
        finishedLoading();
        return;
    }
    cont = response;//make avalable fore debuging
    var content = response;
    window.history.replaceState({ content: content });//add content to history
    var id = content._id;
    displayKmerHeading(content._source);
    results[id] = content._source;
    $('#wikipedia').text(results[id].content);
    finishedLoading();
}

function getGeneLocationData(id) {
    if (results[id].genes) {
        return;
    }

    var URL = m_geneLocationDataUrl + id;
    console.log(URL);
    loading();
    $.getJSON(URL, function (data) {
        //results[id].genes = data;
        results[id].genes = [];
        for (var i in data) {
            results[id].genes[data[i].id] = data[i];
        }

        if (data.length == 0) {
            var str = m_noResultsMsg;
            $('#genes').html("no genes found");
            return;
        }

        var terms = results[id].term_vectors;

        var arr = [];
        var arrData = []
        //todo fix for a gene that is spread over multiple locations
        var j = 0;
        for (var i in terms) {
            while (data[j] && data[j].LocationEnd < terms[i].start_position) {
                j++;
            }
            if (data[j] && data[j].LocationStart <= terms[i].start_position && data[j].LocationEnd >= terms[i].end_position) {
                arr.push(data[j].id);
                arrData.push(terms[i]);
            }
        }

        if (arr.length > 0) {
            getGeneData(arr, id, arrData);
        }

    }).fail(function (jqxhr, textStatus, error) {
        var err = textStatus + ", " + error;
        console.log("Request Failed: " + err);
    }).always(function () {
        finishedLoading();
    });
}

function getGeneData(arr, id, arrData) {
    //todo if zero
    //todo cache

    var URL = m_genesUrl + JSON.stringify(arr);
    console.log(URL);
    loading();
    $.getJSON(URL,
        function (data) { processGeneDataResults(data, arrData); })
    .fail(function (jqxhr, textStatus, error) {
        var err = textStatus + ", " + error;
        console.log("Request Failed: " + err);
    }).always(function () {
        finishedLoading();
    });
}

function processGeneDataResults(data, arrData) {
    $('#GeneData').show();

    if (data.length == 0) {
        var str = m_noResultsMsg;
        $('#genes').html("error with genes");
        return;
    }

    //add to history object
    var histObj = history.state;
    histObj.geneData = data;
    histObj.genes = results[ID].genes;
    histObj.arrData = arrData;
    history.replaceState(histObj);

    //display each gene reult returned
    $.each(data, function (index, val) {
        results[ID].genes[val.id].translation = val.translation;
        results[ID].genes[val.id].LocusTag = val.LocusTag;
        addGeneResult(results[ID].genes[val.id], ID, arrData);
    });

}

function highlight(id) {
    var outHTML = "";
    var content = results[id].content;
    //loop untill no more next kmer
    var previousKmer = results[id].term_vectors[0];
    outHTML += content.substring(0, previousKmer.start_position) + "<span class='hl'>" + content.substring(previousKmer.start_position, previousKmer.end_position) + "</span>";
    for (var i = 1; i < results[id].term_vectors.length; ++i) {
        var currentKmer = results[id].term_vectors[i];
        outHTML += content.substring(previousKmer.end_position, currentKmer.start_position) + "<span class='hl'>" + content.substring(currentKmer.start_position, currentKmer.end_position) + "</span>";
        //todo deal with overlap
        previousKmer = currentKmer;
    }
    outHTML += content.substring(previousKmer.end_position);
    $('#wikipedia').html(outHTML);
}

//adds the give gene reult "data" to the display
function addGeneResult(data, id, arrData) {
    var str = "<div class='media gene-result' id='" + data.id + "' >";
    str += '<div class="media-body"><h4 class="media-heading">' + data.LocusTag + '</h4>';
    str += '<b>Start:</b> ' + data.LocationStart;
    str += '<br /><b>End:</b> ' + data.LocationEnd;
    str += '<br /><b>Translation:</b> ' + data.translation;
    str += '<br /><b>DNA:</b> ' + highlightGeneDNA(results[id].content.substring(data.LocationStart, data.LocationEnd), data.LocationStart, data.LocationEnd, arrData)
    str += "</div></div>";

    $('#genes').append(str);
}

function highlightGeneDNA(content, start, end, arrData) {
    var outHTML = "";

    var arrDataInside = [];
    $.each(arrData, function (index, val) {
        if (start <= val.start_position && end >= val.end_position) {
            var kmer = {};
            kmer.start_position = val.start_position - start;
            kmer.end_position = val.end_position - start;
            arrDataInside.push(kmer);
        }
    });

    //loop until
    var previousKmer = arrDataInside[0];
    outHTML += content.substring(0, previousKmer.start_position) + "<span class='hl'>" + content.substring(previousKmer.start_position, previousKmer.end_position) + "</span>";
    for (var i = 1; i < arrDataInside.length; ++i) {
        var currentKmer = arrDataInside[i];
        outHTML += content.substring(previousKmer.end_position, currentKmer.start_position) + "<span class='hl'>" + content.substring(currentKmer.start_position, currentKmer.end_position) + "</span>";
        //todo deal with overlap
        previousKmer = currentKmer;
    }
    outHTML += content.substring(previousKmer.end_position);
    return outHTML;
}