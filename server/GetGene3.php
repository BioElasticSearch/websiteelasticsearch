<?php
header( 'Content-type: application/json');
//SQL Server Extension Sample Code:
   $connectionInfo = array("UID" => "QUT@f1ub7nda01", "pwd" => "Pass1@34", "Database" => "Bio", "LoginTimeout" => 30, "Encrypt" => 1);
   $serverName = "tcp:f1ub7nda01.database.windows.net,1433";
   $conn = sqlsrv_connect($serverName, $connectionInfo);

$arr = isset($_GET['arr']) ? json_decode($_GET['arr']) : array();

//declare the SQL statement that will query the database
$query = "SELECT id, translation, LocusTag  from gene2 where id in (".implode(',',$arr).")";

$stmt = sqlsrv_query($conn, $query );

$rows = array();
//display the results 
while($row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC))
{
  $rows[] = $row;
}
print json_encode($rows);
//close the connection
sqlsrv_close($conn);
?>