// loading icon
var m_itemsToBeLoaded = 0;

//event once everything has loaded
var loaded = new Event('loaded');

function loading() {
    if (m_itemsToBeLoaded == 0) {
        $.fancybox.showLoading();
    }
    m_itemsToBeLoaded++;
}


function finishedLoading() {
    if (m_itemsToBeLoaded >= 1) {
        m_itemsToBeLoaded--;
    }
    if (m_itemsToBeLoaded == 0) {
        $.fancybox.hideLoading();
        document.dispatchEvent(loaded);
    }
}