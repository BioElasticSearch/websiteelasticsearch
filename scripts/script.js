//ideasm_simplePro
// you tube trailer
// actors

//constants
var m_noResultsMsg = "Sorry, no results found that match your query!";

var SearchTerm = "";
var QueryType = "";
var TermFrequency = "";
var CurrentPage = 0;


$(function () {
	// pressing the enter key on the search box automatically begin the search
	$("#search-box").keyup(function(event){
		if(event.keyCode == 13){
		    $("#btn-header").click();
		}
	});

	$("#btn-header").click(function () {
	    input = $.trim($("#search-box").val()).reduceWhiteSpace();
	    search(input);
	});

    $("#btn-home").click(function () {
        input = $.trim($("#SearchTextArea").val()).reduceWhiteSpace();
        QueryType = $('#QueryModeSelector').val();
        if ($('#CheckboxTermFrequencies').is(':checked')) {
            TermFrequency = $('#TermFrequencies').val();
        } else {
            TermFrequency = null;
        }
		search(input);
    });
	
    $(document).on("click", '.kmer-result', function () {
        var ID = this.id;
        clearPage();
        window.history.pushState(null, null, "/?id=" + ID + "&q=" + SearchTerm + "&t=" + QueryType);
        kmerInfo(ID);
        $('#btn-back').show();
    });

    $(document).on("click", '.page', function () {
        var page = this.id;
        clearPage();
        getPage(page);
    });

    $(".btn-back").click(function () {
        window.history.go(-1);
        clearPage();
        processSearchResult(null, res);
    });

    $('#CheckboxTermFrequencies').change(function () {
        if (this.checked) {
            $('#TermFrequencies').prop('disabled', false);
        } else {
            $('#TermFrequencies').prop('disabled', true);
        }
    });

    $('#QueryModeSelector').change(function () {
        switch (this.value)
        {
            case "match":
                $('#SearchTextArea').attr("placeholder", "Please enter Kmer Search Here");
            case "span":
                $('#CheckboxTermFrequencies').prop('disabled', false);
                if ($('#CheckboxTermFrequencies').is(':checked'))
                {
                    $('#TermFrequencies').prop('disabled', false).css('opacity', 1);
                }
                $('#CheckboxTermFrequenciesLabel').css('color', '');
                $('#TermFrequencies').css('opacity', 1);
                $('#SearchTextArea').attr("placeholder", "Please entor multiple kmers followed by a number representing the maximum distance kmers should be seperated");
                break;
            case "similarity":
            case "similarityFuzzy":
                $('#TermFrequencies').prop('disabled', true).css('opacity', 0.5);
                $('#CheckboxTermFrequencies').prop('disabled', true);
                $('#CheckboxTermFrequenciesLabel').css('color', 'lightgray');
                break;
        }
        switch (this.value) {
            case "match":
                $('#SearchTextArea').attr("placeholder", "Please enter Kmer Search Here");
                break;
            case "span":
                $('#SearchTextArea').attr("placeholder", "Please entor multiple kmers followed by a number representing the maximum distance the kmers should be seperated");
                break;
            case "similarity":
                $('#SearchTextArea').attr("placeholder", "Please enter kmers Here in order to find similer kmers");
                break;
            case "similarityFuzzy":
                $('#SearchTextArea').attr("placeholder", "Please enter kmers Here in order to find similer kmers using fuzzy search");
                break;
        }
    });

    handlePopState();//allways call on page load
    //todo: fix bug history state is always set even on hard refreshes (shift+refresh) so I need to always call handlePopState to load this
    //however when moving forward or back from a different domain it will get hit twice

    $(window).bind('popstate', handlePopState);
});

function handlePopState() {
    clearPage();
    var state = history.state;
    var page = url('?page');
    QueryType = url('?t');
    TermFrequency = url('?tf');
    st = url('?q');
    if (st)
    {
        SearchTerm = decodeURIComponent(st);
    }
    if (page != null) {
        CurrentPage = page;
    }
    if (state)
    {
        if (state.content)
        {
            $('.header').show();
            ID = state.content._id;
            results[ID] = state.content;
            if (state.genes)
            {
                processContentResponse(state.content, state.genes);
                processGeneDataResults(state.geneData, state.arrData);
            }
            else {
                processGetContentResponseNoSearch(null,state.content);
            }
        } else if (state.results) {
            clearPage();
            $('.header').show();
            processSearchResult(null, state.results)
        }

    } else {
        var id = url('?id');
        
        if (id) {
            clearPage();
            $('.header').show();
            if (SearchTerm) {
                $("#search-box").val(SearchTerm);
                $("#SearchTextArea").val(SearchTerm);
            }
            kmerInfo(id);
        } else if (SearchTerm) {
            search(SearchTerm);
            $("#SearchTextArea").val(SearchTerm);
        }
        else {
            clearPage();
            $('#home').show();
        }
    }
}

function getPage(pageNum) {
    var urlExtension = "/?q=" + SearchTerm + "&t=" + QueryType + "&page=" + pageNum;
    $('#kmer-result-list').html("");
    CurrentPage = pageNum;
    switch (QueryType) {
        case "span":
            searchSpanBio(SearchTerm);
            if (TermFrequency) {
                urlExtension += "&tf=" + TermFrequency;
            }
            break;
        case "similarity":
            searchSimiler(SearchTerm);
            break;
        case "similarityFuzzy":
            searchSimilerFuzzy(SearchTerm);
            break;
        case "match":
        default:
            var res = searchBio(SearchTerm);
            if (!res) {
                return false;
            }
            if (TermFrequency) {
                urlExtension += "&tf=" + TermFrequency;
            }
            break;
    }
    window.history.pushState(null, null, urlExtension);
}

function search(querry)
{    
    var urlExtension = "/?q=" + querry + "&t=" + QueryType;
    switch (QueryType) {
        case "span":
            var res = searchSpanBio(querry);
            if (!res) {
                return false;
            }
            if (TermFrequency)
            {
                urlExtension+="&tf="+TermFrequency;
            }
            break;
        case "similarity":
			var res = searchSimiler(querry);
            if (!res) {
                return false;
            }
            break;
        case "similarityFuzzy":
			var res = searchSimilerFuzzy(querry);
            if (!res) {
                return false;
            }
            break;
        case "match":
        default:
            var res = searchBio(querry);
            if (!res) {
                return false;
            }
            if (TermFrequency)
            {
                urlExtension+="&tf="+TermFrequency;
            }
            break;
    }
    clearPage();
    $('.header').show();
    $("#search-box").val(SearchTerm);
    window.history.pushState(null, null, urlExtension);
}

//clears all data of the page so a search can begin
function clearPage() {
    $('#movie-results').hide();
    $('#movie-info').hide();
    $('.jumbotron h1').html("");
    $('.jumbotron .lead').html("");
    $('#kmer-result-list').html("");
    $('.jumbotron .pull-left').html("");
    $('#wikipedia').html("");
	$('#detailed-description').hide();
	$("#wiki-article").attr("href", "");
	trackID = [];
	lyrics = [];
	$('#home').hide();
	$('#pagination-container').hide();
	$('#genes').html("");
	$('#GeneData').hide();
	$('#btn-back').hide();
	$('#kmer-results').hide();
	$('#kmer-info').hide();
	
}

var m_ResultsPerPage = 10;

function AddPagination(numResults) {
    if (numResults > m_ResultsPerPage) {
        $('#pagination-container').show();
        $('#pagination').html("");
        var str = "";
        if (CurrentPage == 0) {
            str += "<li class='disabled'><span>&laquo;</span></li>";
        } else {
            str += "<li class='page' id='" + (+CurrentPage - 1) + "'><span>&laquo;</span></li>";
        }
        for (var i = 0; i < numResults / m_ResultsPerPage; ++i) {
            if (CurrentPage == i) {
                str += "<li class='active page' id='" + i + "'><span>" + (i + 1) + "</span></li>";
            } else {
                str += "<li class='page' id='" + i + "'><span>" + (i + 1) + "</span></li>";
            }
        }
        if (+CurrentPage+1 == Math.ceil(numResults / m_ResultsPerPage)) {
            str += "<li class='disabled'><span>&raquo;</span></li>";
        } else {
            str += "<li class='page' id='" + (+CurrentPage + 1) + "'><span>&raquo;</span></li>";
        }
        $('#pagination').append(str);
    }
}

String.prototype.reduceWhiteSpace = function () {
    return this.replace(/\s+/g, ' ');
};