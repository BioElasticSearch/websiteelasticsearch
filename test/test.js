function runQUnitTests()
{
    m_proxyServerURL = '../server/simple-proxy.php?mode=native&url=';
    testSearch();
    testBasicInfo();
    testWikipediaInfo();
    testReview();
    testSoundTrack();
    testLyrics();
    testTrailer();
}

function testSearch(){
    testNoResults();
    testOneResultReturned();
    testMultipleResults();

    //  good tests:
    //titanic
    //kick-ass
    //  test movies with funny charecters:
    //Sex, Lies, and Videotape
    //One, Two, Three
    //Schindler�s List
    //Dr. Strangelove: How I Learned to Stop Worrying and Love the Bomb
    //Airplane!
    //Fast & Furious 7
    //With a Friend like Harry�
    //(500) Days of Summer
    //9 1/2 Weeks
    //M*A*S*H
    //Romeo + Juliet
    //once
    //the bodyguard
}

function testNoResults()
{
    asyncTest("no results", function () {
        clearPage();
        searchMovies("dalkdjalkdjsalk");
        $(document).one('loaded', function () {
            ok($('#movie-result-list').html() == m_noResultsMsg, "correctly displayed no results string");
            start();
        });
    });
}

function testOneResultReturned()
{
    asyncTest("one results", function () {
        clearPage();
        searchMovies("titanic 1997");
        $(document).one('loaded', function () {
            ok($('.jumbotron h1').html() == "Titanic (1997)", "Correctly displayed only single result");
            start();
        });
    });
}

function testMultipleResults()
{
    asyncTest("multiple results", function () {
        clearPage();
        searchMovies("titanic");
        $(document).one('loaded', function () {
            ok($('.movie-result').length >= 5, "correctly displayed multiple results string");
            start();
        });
    });
}

function testBasicInfo(){
    //TODO
}

function testWikipediaInfo(){
    //TODO
}

function testReview(){
    //TODO
}

function testSoundTrack(){
    //TODO
}

function testLyrics(){
    //TODO
}

function testTrailer() {
    //TODO
}